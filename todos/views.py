from django.shortcuts import render, get_object_or_404
from todos.models import TodoList

# Create your views here.


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_lists": todo_list_list,
    }
    return render(request, "todos/view.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    return render(request, "todos/detail.html", {'todo_list': todo_list})
